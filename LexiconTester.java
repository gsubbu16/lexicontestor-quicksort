//package Assign1;

import java.util.*;
import java.io.*;

public class LexiconTester {

	public static void main(String[] args) throws IOException {

		String file1 = "in1.txt", file2 = "in2.txt";

		Scanner fileInput1 = new Scanner(new File(file1));
		Scanner fileInput2 = new Scanner(new File(file2));

		ArrayList<String> list1 = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> frequency = new ArrayList<String>();
		ArrayList<String> words = new ArrayList<String>();
		ArrayList<String> neigh = new ArrayList<String>();
		ArrayList<String> write = new ArrayList<String>();

		// if file is empty
		if (!fileInput1.hasNextLine() || !fileInput2.hasNextLine()) {
			if (!fileInput1.hasNextLine())
				System.out.println(fileInput1 + "is an empty file");
			if (!fileInput2.hasNextLine())
				System.out.println(fileInput2 + "is an empty file");
		}
		// if file is not empty
		else {
			while (fileInput1.hasNext()) {
				// removing punctuation & numbers
				list1.add(fileInput1.next().toLowerCase().replaceAll("[\\p{Punct}\\d]", ""));
				list1.remove("");
			}
			while (fileInput2.hasNext()) {
				// removing punctuation & numbers
				list2.add(fileInput2.next().toLowerCase().replaceAll("[\\p{Punct}\\d]", ""));
				list2.remove("");
			}

			// combining the lists from both the files
			list1.addAll(list2);

			for (int i = 0; i < list1.size(); i++) {
				int count = 1;
				for (int j = i + 1; j < list1.size(); j++) {
					// removing duplicates from the list
					if (list1.get(i).equalsIgnoreCase(list1.get(j))) {
						count++;
						list1.remove(j);
						j--;
					}

				}
				// list containing only words
				words.add(list1.get(i));
				
				// list containing words with frequency
				frequency.add(list1.get(i) + " " + count);

			}

			// select the sorting algorithm
			if (args.length == 1 && args[0].equals("Y")) {
				bubbleSort(words);
				//System.out.println("bubble sort");
			} else {
				quickSort(words, 0, words.size() - 1);
				//System.out.println("quick sort");
			}		

			for (int i = 0; i < words.size(); i++) {
				for (int j = words.size() - 1; j >= 0; j--) {
					if (words.get(i).length() == words.get(j).length()) {
						int check = 0;
						for (int k = 0; k < words.get(i).length(); k++) {
							if (words.get(i).charAt(k) != words.get(j).charAt(k))
								check++;
						}

						// add to neighbours if they differ by only 1 character
						if (check == 1)
							neigh.add(words.get(j));
						// reset the value
						check = 0;
					}
				}

				// select the sorting algorithm
				if (args.length == 1 && args[0].equals("Y")) {
					bubbleSort(neigh);
					bubbleSort(frequency);
				} else {
					quickSort(neigh, 0, neigh.size() - 1);
					quickSort(frequency, 0, frequency.size() - 1);
				}
				System.out.println(frequency.get(i) + " " + neigh);
				write.add(frequency.get(i) + " " + neigh);
				
				// output the list to file
				writeToFile(write);
				
				neigh = new ArrayList<String>();
			}
		}

		fileInput1.close();
		fileInput2.close();

	}

	public static void bubbleSort(ArrayList<String> list) {

		int left = 0;
		int right = list.size() - 1;

		for (int i = right; i >= left + 1; i--) {
			for (int j = left; j <= i - 1; j++) {
				if (list.get(j).compareTo(list.get(j + 1)) > 0) {
					swap(list, j, j + 1);
				}
			}
		}

	}

	public static void quickSort(ArrayList<String> list, int left, int right) {

		if (left < right) {
			int pivotIndex = partition(list, left, right);
			quickSort(list, left, pivotIndex - 1);
			quickSort(list, pivotIndex + 1, right);
		}
	}

	public static int partition(ArrayList<String> list, int left, int right) {
		int mid = (left + right) / 2;
		String pivot = list.get(mid);
		swap(list, mid, right);

		while (left < right) {
			while (left < right && list.get(left).compareTo(pivot) <= 0) {

				left++;
			}
			if (left < right) {
				swap(list, left, right);
			}
			while (left < right && list.get(right).compareTo(pivot) >= 0) {
				right--;
			}
			if (left < right) {
				swap(list, left, right);
				left++;
			}
		}
		return left;
	}

	private static <E> void swap(AbstractList<E> list, int i, int j) {
		E temp = list.get(i);
		list.set(i, list.get(j));
		list.set(j, temp);
	}

	public static void writeToFile(ArrayList<String> list) {
		try (BufferedOutputStream bs = new BufferedOutputStream(new FileOutputStream("out.txt"))) {
			String fileContent = "";
			for (int i = 0; i < list.size(); i++) {
				fileContent = list.get(i) + "\n";
				bs.write(fileContent.getBytes());
			}
		} catch (IOException e) {
			System.out.println("Error:Cannot write to file");
			e.printStackTrace();
		}
	}

}
